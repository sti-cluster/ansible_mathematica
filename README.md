# Role Mathematica

Ansible role to install silently Mathematica 12 without licenses. Other versions will come later

Compatibility
------------

This role can be used on all Linux OS

Variables Used
------------

You can find in defaults/main.yml all variables used in tasks

| Variable       | Default Value               | Type   | Description                        |
| -------------- | :-------------------------- | :----- | ---------------------------------- |
| mathematica_sh | Mathematica_12.0.0_LINUX.sh | String | Mathematica installer              |
| dest_folder    | /tmp                        | String | Path where the installer is stored |

## Activating license

To activate the license, you must to connect via ssh via your export display like:

```bash
ssh username@serverhostname -X
```

Then you'll have to start mathematica by using this command:

```bash
mathematica
```

In the opened windows, you'll have to fill with your informations & activation key.

### Get an activation key

You have to connect on go.epfl.ch/wolfram. Then connect with your gaspar account. You'll be able then to activate an activation key on your user portal 

## Author Information

Written by [Dimitri Colier](mailto:dimitri.colier@epfl.ch) for EPFL - STI school of engineering